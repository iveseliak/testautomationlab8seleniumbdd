package com.gmail.businessObject;

import com.gmail.pom.GmailPageObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class GmailMoveMsgToImp {
    public static final Logger LOG = LogManager.getLogger(GmailMoveMsgToImp.class);

    private GmailPageObject gmailPageObject;

    public GmailMoveMsgToImp(WebDriver driver){
        gmailPageObject = new GmailPageObject(driver);
    }

    public void moveToImpFolder(){
        gmailPageObject.getSelectMessage();
        gmailPageObject.getOptionsButton().click();
        gmailPageObject.getImportantBtn().click();
    }

    public boolean varifyMoveToImpFolder() {
        if(gmailPageObject.getImportantAlert().check() == true)
            LOG.info("Message moved to important folder");
        else
            LOG.error("Message didn`t move to important folder");
        return gmailPageObject.getImportantAlert().check();

    }

    public void deleteImpMessage(){

        gmailPageObject.getDeleteBtn().click();
    }

    public boolean varifyDeleteMessage() {
        if (gmailPageObject.getDeleteAlert().check() == true)
            LOG.info("Message deleted. Test end successful");
         else
            LOG.error("Test failed");

            return gmailPageObject.getDeleteAlert().check();

        }

}
