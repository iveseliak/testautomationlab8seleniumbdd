package com.gmail.componet;


import com.gmail.decorator.Element;
import org.openqa.selenium.WebElement;

public class Alert extends Element {
    public Alert(WebElement webElement) {
        super(webElement);
    }
    public boolean check() {
        return webElement.isDisplayed();
    }
}
