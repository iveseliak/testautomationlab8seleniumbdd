package com.gmail.pom;

import com.gmail.componet.Alert;
import com.gmail.componet.Button;
import com.gmail.componet.TextInput;
import com.gmail.decorator.CustomFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GmailPageObject {

    public static final Logger LOG = LogManager.getLogger(GmailPageObject.class);
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "identifierId")
    private TextInput loginInput;

    @FindBy(xpath = "//div[@id='identifierNext']")
    private Button nextButton;

    @FindBy(xpath = "//div//input[@type='password']")
    private TextInput passwordInput;

    @FindBy(xpath = "//div[@id='passwordNext']")
    private Button nextPassButton;

    @FindBy(xpath = "//div[@class='oZ-jc T-Jo J-J5-Ji ' and @role='checkbox']")
    private List<WebElement> selectMessage;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji mA nf T-I-ax7 L3' and @role='button']//span[@class='asa bjy']")
    private Button optionsButton;

    @FindBy(xpath = "//div[@class='J-M aX0 aYO jQjAxd' and @role='menu']//div[@class='SK AX']//div[4]//div[1]")
    private Button importantBtn;

    @FindBy(xpath = "//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA' and @role='button']//div[@class='asa']")
    private Button deleteBtn;

    @FindBy(xpath = "//div[@class='vh']//span[@class='aT']")
    private Alert importantAlert;

    @FindBy(xpath = "//div[@class='vh']//span[@class='aT']")
    private Alert deleteAlert;

    public GmailPageObject(WebDriver driver) {
        PageFactory.initElements(new CustomFieldDecorator(driver), this);
        this.driver = driver;
    }

    public TextInput getLoginInput(){return loginInput;}

    public  Button getNextButton(){return nextButton;}

    public TextInput getPasswordInput(){return  passwordInput;}

    public Button getNextPassButton(){return nextPassButton;}

    public void getSelectMessage(){selectMessage();}

    public Button getOptionsButton(){return optionsButton;}

    public Button getImportantBtn(){return importantBtn;}

    public Button getDeleteBtn(){return deleteBtn;}

    public Alert getImportantAlert(){return importantAlert;}

    public Alert getDeleteAlert(){return deleteAlert;}

    public void selectMessage() {
        for (int i = 0; i < 3; i++) {
            selectMessage.get(i).click();
        }
        LOG.info("Three message selected");
    }

}
